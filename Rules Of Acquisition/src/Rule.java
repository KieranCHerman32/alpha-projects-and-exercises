public class Rule {
    private int ruleNum;
    private String ruleText;

    public Rule(int ruleNum, String ruleText) {
        this.ruleNum = ruleNum;
        this.ruleText = ruleText;
    }

    public int getRuleNum() {
        return ruleNum;
    }

    public void setRuleNum(int ruleNum) {
        this.ruleNum = ruleNum;
    }

    public String getRuleText() {
        return ruleText;
    }

    public void setRuleText(String ruleText) {
        this.ruleText = ruleText;
    }
}
