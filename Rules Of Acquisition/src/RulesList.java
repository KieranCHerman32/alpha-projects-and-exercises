import java.util.HashMap;
import java.util.Map;

public class RulesList {
    public Map<Integer, String> rules;

    public void generateDefaultList() {
        rules = new HashMap<>();
        rules.put(0, "When no appropriate rule applies, make one up.");
        rules.put(1, "Once you have their money, never give it back.");
        rules.put(2, "Money is everything.");
        rules.put(3, "Never spend more for an acquisition than you have to.");
        //
        rules.put(5, "Always exaggerate your estimates.");
        rules.put(6, "Never allow family to stand in the way of opportunity.");
        rules.put(7, "Keep your ears open.");
        rules.put(8, "Small print leads to large risk.");
        rules.put(9, "Opportunity + Instinct = Profit.");
        rules.put(10, "Greed is eternal.");
        //
        rules.put(13, "Anything worth doing is worth doing for money.");
        rules.put(14, "Sometimes the quickest way to find profits is to let them find you.");
        rules.put(15, "Dead mean close no deals.");
        rules.put(16, "A deal is a deal; Until a better one comes along.");
        rules.put(17, "A contract is a contract is a contract (but only between Ferengi).");
        rules.put(18, "A Ferengi without profit is no Ferengi at all.");
        rules.put(19, "Satisfaction is not guaranteed");
        rules.put(20, "He who dives under the table today lives to profit tomorrow.");
        rules.put(21, "Never place friendship above profit.");
        rules.put(22, "A wise man can hear profit in the wind.");
        rules.put(23, "Nothing is more important than your health (except for your money).");
        //
        rules.put(27, "There's nothing more dangerous than an honest businessman.");
        //
        rules.put(29, "Always ask \"What's in it for me?\"");
        rules.put(30, "Confidentiality equals profit.");
        //
        rules.put(31, "Never make fun of a Ferengi's mother. Insult something he cares about instead.");
        //
        rules.put(33, "It never hurts to suck up to the boss.");
        rules.put(34, "War is good for business.");
        rules.put(35, "Peace is good for business.");
        //
        rules.put(37, "The early investor reaps the most profit");
        //
        rules.put(39, "Don't tell customers more than they need to know.");
        rules.put(40, "She can touch your lobes but never your latinum.");
        rules.put(41, "Profit is its' own reward.");
        //
        rules.put(43, "Feed your greed, but not enough to choke it.");
        rules.put(44, "Never confuse wisdom with luck.");
        //
        rules.put(47, "Don't trust a man wearing a better suit than your own.");
        rules.put(48, "The bigger the smile, the sharper the knife.");
        //
        rules.put(52, "Never ask when you can take.");
        rules.put(53, "Never trust anybody taller than you.");
        rules.put(54, "Rate / Time = Profit. (The Velocity of Wealth)");
        rules.put(55, "Take joy from profit, and profit from joy.");
        //
        rules.put(57, "Good customers are as rare as latinum. Treasure them.");
        rules.put(58, "There is no substitute for success.");
        rules.put(59, "Free advice is seldom cheap.");
        rules.put(60, "Keep your lies consistent.");
        //
        rules.put(62, "The riskier the road, the greater the profit.");
        rules.put(63, "Work is the best therapy for your employees.");
        //
        rules.put(65, "Win or lose, there's always Huyperian Beetle snuff.");
        rules.put(66, "Someone's always got bigger ears.");
        //
        rules.put(68, "Risk doesn't always equal reward.");
        rules.put(69, "Ferengi are not responsible for the stupidity of other races.");
        //
        rules.put(74, "Knowledge equals profit.");
        rules.put(75, "Home is where the heart it, but the stars are made of latinum.");
        rules.put(76, "Every once in a while, declare peace. It confuses the hell out of your enemies.");
        rules.put(77, "If you break it, you'll be charged for it.");
        //
        rules.put(79, "Beware the Vulcan greed for knowledge.");
        //
        rules.put(82, "The flimsier the product, the higher the price.");
        //
        rules.put(85, "Never let the competition know what you're thinking.");
        //
        rules.put(87, "Learn the customer's weakness so that you may better take advantage of him");
        //
        rules.put(89, "Ask not what your profits can do for you, but what you can do for your profits.");
        //
        rules.put(92, "There are many paths to profit.");
        //
        rules.put(94, "Females and finance don't mix.");
        rules.put(95, "Expand or die.");
        //
        rules.put(97, "Enough is never enough.");
        rules.put(98, "Every man has his price.");
        rules.put(99, "Trust is the biggest liability of all.");
        rules.put(100, "When it's good for business, tell the truth.");
        rules.put(101, "Profit trumps emotion.");
        rules.put(102, "Nature decays but latinum lasts forever.");
        rules.put(103, "Sleep can interfere with opportunity.");
        rules.put(104, "Faith moves mountains of inventory.");
        //
        rules.put(108, "A woman wearing clothes is like a man without any profit.");
        rules.put(109, "Dignity and an empty sack is worth the sack.");
        rules.put(110, "Exploitation begins at home.");
        rules.put(111, "Treat people in your debt like family; Exploit them.");
        rules.put(112, "Never have sex with the boss's sister.");
        rules.put(113, "Always have sex with the boss.");
        //
        rules.put(121, "Everything is for sale; Including friendship.");
        //
        rules.put(125, "You can't make a deal if you're dead.");
        //
        rules.put(135, "Listen to secrets but never repeat them.");
        //
        rules.put(139, "Wives serve. Brothers inherit.");
        //
        rules.put(141, "Only fools pay retail.");
        //
        rules.put(144, "There's nothing wrong with charity as long as it ends up in your pocket.");
        //
        rules.put(147, "People love the bartender.");
        //
        rules.put(151, "Even when you're the customer, sell yourself.");
        //
        rules.put(153, "Sell the sizzle, not the steak.");
        //
        rules.put(162, "Even in the worst of times, someone turns a profit.");
        //
        rules.put(168, "Whisper your way to success.");
        //
        rules.put(177, "Know your enemies but do business with them anyways.");
        //
        rules.put(181, "Not even dishonesty can tarnish the shine of profit.");
        //
        rules.put(183, "When life hands you ungaberries, make detergent.");
        rules.put(184, "A Ferengi waits to bid until his opponents have exhausted themselves.");
        //
        rules.put(189, "Let others keep their reputation; You keep their money.");
        rules.put(190, "Hear all, trust nothing.");
        //
        rules.put(192, "Never cheat a Klingon unless you're sure you can get away with it.");
        rules.put(193, "Trouble comes in threes.");
        rules.put(194, "It's always good business to know about new customers before they walk in the door.");
        //
        rules.put(199, "Location, location, location.");
        rules.put(200, "A Ferengi chooses no side but his own.");
        //
        rules.put(203, "New customers are like razor-toothed Gree-worms. They can be succulent, but sometimes they bite back.");
        //
        rules.put(208, "Sometimes the only thing more dangerous than a question is an answer.");
        //
        rules.put(211, "Employees are the rungs on the ladder of success. Don't hesitate to step on them.");
        rules.put(212, "A good lie is easier to believe than the truth.");
        //
        rules.put(214, "Never begin a business negotiation on an empty stomach.");
        //
        rules.put(216, "Never gamble with a telepath.");
        rules.put(217, "You can't free a fish from water.");
        rules.put(218, "Sometimes what you get free costs entirely too much.");
        rules.put(219, "Possession is 11/10ths of the law.");
        //
        rules.put(223, "Beware the man who doesn't make time for Oo-mox.");
        //
        rules.put(227, "If that's what's written, then that's what's written.");
        //
        rules.put(229, "Latinum lasts longer than lust.");
        //
        rules.put(235, "Duck; Death is tall.");
        rules.put(236, "You can't buy fate.");
        //
        rules.put(239, "Never be afraid to mislabel a product.");
        rules.put(240, "Time - like latinum - is a highly prized commodity.");
        //
        rules.put(242, "More is good, all is better.");
        rules.put(243, "Always leave yourself an out.");
        //
        rules.put(255, "A wife is a luxury. A smart accountant is a necessity.");
        //
        rules.put(257, "When the messenger comes to appropriate your profits, kill the messenger.");
        //
        rules.put(261, "A wealthy man can afford everything except a conscious.");
        //
        rules.put(263, "Never allow doubt to tarnish your lust for latinum.");
        //
        rules.put(266, "When in doubt, lie.");
        rules.put(267, "If you believe it, they believe it.");
        //
        rules.put(272, "Always inspect the merchandise before making a deal.");
        //
        rules.put(280, "If it ain't broke, don't fix it.");
        //
        rules.put(284, "Deep down, everyone's a Ferengi.");
        rules.put(285, "No good deed ever goes unpunished.");
        rules.put(286, "When Morn leaves, it's all over.");
        rules.put(287, "Always get somebody else to do the lifting.");
        rules.put(288, "Never get into anything that you can't get out of.");
        rules.put(289, "A man is only worth the sum of his possessions.");
        rules.put(290, "An angry man is an enemy. A satisfied man is an ally.");
        rules.put(291, "The less employees know about the cash flow, the smaller the share they can afford.");
        rules.put(292, "Only a fool passes up a business opportunity.");
        rules.put(293, "The more time they take deciding, the more money they will spend.");
        rules.put(294, "A bargain usually isn't.");
        //
        rules.put(299, "After you've exploited someone, it never hurts to thank you. That way, it is easier to exploit them again.");
        //
        rules.put(431, "When the shooting starts, let the mercenaries handle it.");
    }

}
