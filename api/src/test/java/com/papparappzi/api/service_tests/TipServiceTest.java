package com.papparappzi.api.service_tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.papparappzi.api.Services.TipService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

// Acceptance Criteria
// * Given: a party size of 6 or greater, 
//   When: any level of customer satisfaction, 
//   Then set tip percentage to 20% and calculate total bill

// * Given: a party size of less than 6, 
//   When: customer satisfaction = 3 (highest), 
//   Then set tip percentage to 22% and calculate total bill

// * Given: a party size of less than 6, 
//   When: customer satisfaction = 2 (good, but not great), 
//   Then set tip percentage to 20% and calculate total bill

// * Given: a party size of less than 6, 
//   When: customer satisfaction = 1 (meh), 
//   Then set tip percentage to 18% and calculate total bill

// * Given: a party size of less than 6, 
//   When: customer satisfaction = 0 (miserable), 
//   Then set tip percentage to 0% and calculate total bill

// * Given: any call to calcTip, 
//   When: party size is less than zero, 
//   Then throw an exception

// * Given: any call to calcTip, 
//   When: party size  is greater than three hundred, 
//   Then throw an exception

@SpringBootTest
public class TipServiceTest {
    @Autowired
    private TipService tipSvc;

    @Test
    void GivenPartySizeOfSix_WhenAnyCustomerSatisfaction_ThenReturnBillPlusTwentyPercent() {
        assertEquals(120f, tipSvc.calcTip(6, 0, 100f), "Calc Tip Failed");
    }

    @Test
    void GivenPartySizeLessThanSix_WhenCustomerSatisfactionIsThree_ThenReturnBillPlusTwentyTwoPercent() {
        assertEquals(122f, tipSvc.calcTip(5, 3, 100f), "Calc Tip Failed");
    }

    @Test
    void GivenPartySizeLessThanSix_WhenCustomerSatisfactionIsTwo_ThenReturnBillPlusTwentyPercent() {
        assertEquals(120f, tipSvc.calcTip(5, 2, 100f), "Calc Tip Failed");
    }

    @Test
    void GivenPartySizeLessThanSix_WhenCustomerSatisfactionIsOne_ThenReturnBillPlusEighteenPercent() {
        assertEquals(118f, tipSvc.calcTip(5, 1, 100f), "Calc Tip Failed");
    }

    @Test
    void GivenPartySizeLessThanSix_WhenCustomerSatisfactionIsZero_ThenReturnBillPlusZeroPercent() {
        assertEquals(100f, tipSvc.calcTip(5, 0, 100f), "Calc Tip Failed");
    }

    @Test
    void GivenCallToCalcTip_WhenPartySizeIsLessThanZero_ThenThrowException() {
        assertThrows(IllegalArgumentException.class, () -> {tipSvc.calcTip(-1, 0, 200f);});
    }

    @Test
    void GivenCallToCalcTip_WhenPartySizeIsGreaterThanThreeHundred_ThenThrowException() {
        assertThrows(IllegalArgumentException.class, () -> {tipSvc.calcTip(301, 0, 200f);});
    }

    @Test
    void GivenCallToCalcTip_WhenBillIsLessThanZero_ThenThrowException() {
        assertThrows(IllegalArgumentException.class, () -> {tipSvc.calcTip(5, 0, 0f);});
    }
}
