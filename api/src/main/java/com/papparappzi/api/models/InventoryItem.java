package com.papparappzi.api.models;

public class InventoryItem {
    private long id;

    private String description;

    private long count;
    
    private float costPerItem;
    
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public float getCostPerItem() {
		return costPerItem;
	}

	public void setCostPerItem(float costPerItem) {
		this.costPerItem = costPerItem;
	}

	public InventoryItem(long id, String description, long count, float costPerItem) {
		this.id = id;
		this.description = description;
		this.count = count;
		this.costPerItem = costPerItem;
	}

}