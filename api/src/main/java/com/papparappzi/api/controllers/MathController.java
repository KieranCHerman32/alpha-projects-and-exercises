package com.papparappzi.api.controllers;

import com.papparappzi.api.models.MathRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("math")
public class MathController {
    
    @GetMapping("add")
    public float add(@RequestParam(name = "num1") float num1, @RequestParam(name = "num2") float num2){
        float result = num1 + num2;
        return result;
    }

    @GetMapping("/subtract/{num1}/{num2}")
    public float subtract(@PathVariable("num1") float num1, @PathVariable("num2") float num2) {
        return num1 - num2;
    }

    @PostMapping("multi")
    public float multiply(@RequestBody MathRequest requestValues){
        return requestValues.getNum1() * requestValues.getNum2();
    }
}