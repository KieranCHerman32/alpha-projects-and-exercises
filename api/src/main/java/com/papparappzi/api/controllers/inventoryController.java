package com.papparappzi.api.controllers;

import java.util.ArrayList;
import java.util.List;

import com.papparappzi.api.models.InventoryItem;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "inventory")
public class InventoryController {
    @GetMapping("all")
    @ResponseBody
    public List<InventoryItem> getInventory(){
        List<InventoryItem> inventoryItems = new ArrayList<InventoryItem>();
        inventoryItems.add(new InventoryItem(2, "Apples", 3, 2.24f));
        return inventoryItems;
    }
}