package com.papparappzi.api.controllers;

import java.util.List;

import com.papparappzi.api.models.File;
import com.papparappzi.api.models.FileUploadRequest;
import com.papparappzi.api.repositories.FileRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@RestController
@RequestMapping("/filestack")
public class FileStackController {

    @Autowired
    private FileRepository fileRepo;

    @PostMapping("/upload")
    public ResponseEntity<File> uploadFile(@RequestBody FileUploadRequest requestValues) {
        // Validate input
        if (
        requestValues == null ||
        requestValues.getName() == null || requestValues.getName().length() <= 0 ||
        requestValues.getDescription() == null || requestValues.getDescription().length() <= 0 || 
        requestValues.getUrl() == null || requestValues.getUrl().length() <= 0 
        ){
            return ResponseEntity.badRequest().build();
        }
        // Declare DB object
        File fileToSave = new File();
        // Assign values to DB Object
        fileToSave.setName(requestValues.getName());
        fileToSave.setDescription(requestValues.getDescription());
        fileToSave.setUrl(requestValues.getUrl());
        // Save DB Object into the database and store created record in the "savedFile" object
        File savedFile = fileRepo.save(fileToSave);
        // Return the saved object to the caller
        return ResponseEntity.ok(savedFile);
    } 

    @GetMapping("/files")
  public @ResponseBody Iterable<File> getAllUsers() {
    // This returns a JSON or XML with the users
    return fileRepo.findAll();
  }
    
}