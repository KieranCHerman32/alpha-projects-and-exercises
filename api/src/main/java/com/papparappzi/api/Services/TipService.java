package com.papparappzi.api.Services;

import org.springframework.stereotype.Service;

@Service
public class TipService {

    public float calcTip(int partySize, int customerSat, float billAmt) {
        float tipPercent;
        if (billAmt <= 0) {
            throw new IllegalArgumentException("Invalid Bill Value");
        } else if (partySize < 0 || partySize > 300) {
            throw new IllegalArgumentException("Invalid Party Size");
        } else if (partySize >= 6) {
            tipPercent = .20f;
        } else if (customerSat == 3) {
            tipPercent = .22f;
        } else if (customerSat == 2) {
            tipPercent = .20f;
        } else if (customerSat == 1) {
            tipPercent = .18f;
        } else {
            tipPercent = 0f;
        }
        return updateBill(billAmt, tipPercent);
    }

    private float updateBill(float billAmt, float tipPercent) {
        return billAmt + (billAmt * tipPercent);
    }
}