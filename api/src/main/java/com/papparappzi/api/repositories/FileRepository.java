package com.papparappzi.api.repositories;

import com.papparappzi.api.models.File;

import org.springframework.data.repository.CrudRepository;

public interface FileRepository extends CrudRepository<File, Long> {
}