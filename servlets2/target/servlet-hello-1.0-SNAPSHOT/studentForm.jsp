<%--
  Created by IntelliJ IDEA.
  User: student04
  Date: 4/8/20
  Time: 1:39 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student Form</title>
</head>
<body>
<form action="studentFormResponse.jsp" method="get">
    <p>First Name: </p>
    <input type="text" name="firstName" id="fName">
    <br>
    <p>Last Name: </p>
    <input type="text" name="lastName" id="lName">
    <br>
    <select name="gender" id="gender">
        <option value="m">Male or Male-presenting</option>
        <option value="f">Female or Female-presenting</option>
        <option value="n">Non-binary</option>
    </select>
    <p>What Programming Languages do you already know?</p>
    <input type="checkbox" name="progLang" value="java">Java
    <input type="checkbox" name="progLang" value="js">JavaScript
    <input type="checkbox" name="progLang" value="ang">Angular
    <input type="checkbox" name="progLang" value="php">PHP

    <button type="submit">Create Account</button>

</form>
</body>
</html>
