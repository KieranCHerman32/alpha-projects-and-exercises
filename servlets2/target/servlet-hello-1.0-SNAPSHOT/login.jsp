<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<jsp:include page="./WEB-INF/partials/navbar.jsp"/>
<form action="profile.jsp" method="post">
    <label for="username">
        Username
        <input type="text" id="username">
    </label>
    <label for="password">
        Password
        <input type="password" id="password">
    </label>
    <button type="submit">Log Me in</button>
</form>
<jsp:include page="./WEB-INF/partials/footer.jsp"/>
</body>
</html>
