<%--
  Created by IntelliJ IDEA.
  User: student04
  Date: 4/9/20
  Time: 4:18 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create-a-Product</title>
    <%@include file="WEB-INF/partials/head.jsp"%>
</head>
<body>
<form action="/products/create" method="post">
    <label for="name">Name</label>
    <input type="text" name="name" id="name">
    <br>
    <label for="price">Price</label>
    <input type="text" name="price" id="price">
    <br>
    <input type="submit">
</form>
<%@include file="WEB-INF/partials/backButton.jsp"%>
</body>
</html>
