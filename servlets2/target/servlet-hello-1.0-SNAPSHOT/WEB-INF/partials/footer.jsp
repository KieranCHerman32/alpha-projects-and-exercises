<%--
  Created by IntelliJ IDEA.
  User: student04
  Date: 4/7/20
  Time: 10:26 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
<%--    <%@include file="head.jsp"%>--%>
    <style>
        body {
            margin: 0;
            font-family: Arial, Helvetica, sans-serif;
        }

        .footer {
            overflow: hidden;
            background-color: #333;
            height: 5%;
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
        }

        .footer a {
            float: left;
            color: #f2f2f2;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
        }
    </style>
</head>
<body>
<div class="footer">
    <a href="#">Twitter @Footer</a>
    <a href="#">Facebook</a>
    <a href="#">LinkedIn</a>
</div>
</body>
</html>
