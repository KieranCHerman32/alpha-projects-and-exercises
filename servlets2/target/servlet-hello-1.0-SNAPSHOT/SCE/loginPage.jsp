<%--
  Created by IntelliJ IDEA.
  User: student04
  Date: 4/10/20
  Time: 3:40 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Log In</title>
    <%@include file="../WEB-INF/partials/head.jsp" %>
</head>
<body>
<%@include file="../WEB-INF/partials/navbar.jsp" %>
<div class="container">
    <div class="card my-5">
        <div class="card-body">
            <form action="/SCE/masterRepo.jsp" method="post">
                <div class="row">
                    <label for="username" class="mx-auto">
                        Username
                        <input class="form-control mx-auto" type="text" name="username" id="username">
                    </label>
                </div>
                <div class="row">
                    <label for="password" class="mx-auto">
                        Password
                        <input class="form-control mx-auto" type="password" name="password" id="password">
                    </label>
                </div>
                <div class="row">
                    <button type="submit" class="btn btn-outline-success mx-auto">
                        Git Logged In
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<%@include file="../WEB-INF/partials/footer.jsp" %>
</body>
</html>
