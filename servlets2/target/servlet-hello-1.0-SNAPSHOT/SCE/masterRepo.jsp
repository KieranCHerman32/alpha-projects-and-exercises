<%--
  Created by IntelliJ IDEA.
  User: student04
  Date: 4/10/20
  Time: 3:54 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin's Profile</title>
    <%@include file="../WEB-INF/partials/head.jsp" %>
</head>
<body>
<%@include file="../WEB-INF/partials/navbar.jsp" %>

<div class="container">
    <div class="card my-5">
        <div class="card-header">
            Welcome to Admin's Profile
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <div class="mx-auto">
                                Product Listing
                            </div>
                        </div>
                        <div class="card-body">
                            <a class="btn btn-block btn-outline-success" href="/products" role="button">Go To
                                Project</a>
                        </div>
                        <div class="card-footer"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <div class="mx-auto">
                                Student Registration
                            </div>
                        </div>
                        <div class="card-body">
                            <a class="btn btn-block btn-outline-success" href="/myForm.jsp" role="button">Go To
                                Project</a>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <div class="mx-auto">
                                CodeBound Curbside
                            </div>
                        </div>
                        <div class="card-body">
                            <a class="btn btn-block btn-outline-success" href="/curbside" role="button">Go To
                                Project</a>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="../WEB-INF/partials/footer.jsp" %>
</body>
</html>
