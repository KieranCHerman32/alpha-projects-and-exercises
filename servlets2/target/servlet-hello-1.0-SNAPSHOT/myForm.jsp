<%--
  Created by IntelliJ IDEA.
  User: student04
  Date: 4/8/20
  Time: 2:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>My Form</title>
    <jsp:include page="/WEB-INF/partials/head.jsp"/>
</head>
<body>
<%@include file="WEB-INF/partials/navbar.jsp" %>
<div class="container">
    <div class="card my-5">
        <div class="card-body">
            <form action="myFormResponse.jsp" method="get">
                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <label>First Name</label>
                            <input class="form-control" type="text" name="firstName">
                        </div>
                        <div class="col">
                            <label>Last Name</label>
                            <input class="form-control" type="text" name="lastName">
                        </div>
                    </div>
                </div>
                <div class="form-group card">
                    <label class="m-auto">GARDEN of choice</label>
                    <div class="form-check card m-2 p-0">
                        <div class="card-header">
                            <input class=" form-check-input ml-1" type="radio" name="garden" value="Balamb">
                            <label class="ml-4"> Balamb</label>
                        </div>
                        <div class="card-body">
                            While located within the island-nation of Balamb, Balamb GARDEN SeeD are not considered
                            agents
                            of
                            the Balamb government, and act instead as mercenary units. Requests for Balamb Garden Aid
                            are
                            carefully vetted to ensure adherence to the Balamb GARDEN Code of Ethics before being
                            assigned
                            to
                            SeeD teams to carry out. Despite Balamb GARDEN's independence, they maintain a close
                            relationship
                            with the Balamb government, with Balamb SeeD often requested to serve as representives of
                            the
                            Island
                            Nation - should said request comply with the GARDEN's code of ethics.
                        </div>
                    </div>
                    <div class="form-check card m-2 p-0">
                        <div class="card-header">
                            <input class="form-check-input ml-1" type="radio" name="garden" value="Galbadia">
                            <label class="ml-4">Galbadia</label>
                        </div>
                        <div class="card-body">
                            Located northwest of the Monterosa Plateau in central Galbadia; Graduates of Galbadia GARDEN
                            are
                            contracted to accept either: An Officer's position within the Galbadian Military or
                            admittance
                            to
                            Galdadia's branch of SeeD. Unlike Balamb SeeD, Galbiadia SeeD are considered agents of the
                            state
                            and
                            must swear an oath to always act - in all things - in the best interest of Galbadia.
                            Galbadia
                            SeeD
                            are typically offered assignments that allow them to act as a direct extension - or
                            representative -
                            of the Galbadian military.
                        </div>
                    </div>
                    <div class="form-check card m-2 p-0">
                        <div class="card-header">
                            <input class="form-check-input ml-1" type="radio" name="garden" value="Trabia">
                            <label class="ml-4">Trabia</label>
                        </div>
                        <div class="card-body">
                            Nestled in the Bika Snowfield, Trabia GARDEN acts as both a traditional school for children
                            ages
                            6 - 18 as well as a 'proper' GARDEN who's graduates go on to join their country's branch of
                            SeeD.
                            Trabia GARDEN's official motto is "Take Care of Oneself", while the GARDEN itself operates
                            under
                            a
                            different - albeit similar - maxim; "Take Care of Eachother". Trabia SeeD operate much
                            closer in
                            principle to a world relief force than mercenary units, and they rarely accept requests for
                            military
                            action or intervention.
                        </div>
                    </div>
                </div>
                <label class="m-auto">Weapons Training Tract</label>
                <select class="form-control" name="weapon" id="weapon">
                    <option value="Undecided">Undecided</option>
                    <option value="Chakram">Chakram</option>
                    <option value="Quarterstaff">Quarterstaff</option>
                    <option value="Gunblade">Gunblade</option>
                    <option value="BlasterEdge">Blaster Edge</option>
                    <option value="Gauntlets">Gauntlet(s)</option>
                    <option value="Nunchaku">Nunchaku</option>
                    <option value="Whip">Whip</option>
                    <option value="Firearms">Firearms</option>
                    <option value="Katar">Katar</option>
                    <option value="Lance">Lance</option>
                </select>
                <button class="btn btn-primary mx-auto" type="submit">Confirm Registration</button>
            </form>
        </div>
        <%@include file="WEB-INF/partials/backButton.jsp" %>
    </div>
</div>
<%@include file="WEB-INF/partials/footer.jsp" %>
</body>
</html>
