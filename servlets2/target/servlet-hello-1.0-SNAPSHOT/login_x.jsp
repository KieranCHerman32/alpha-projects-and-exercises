<%--
  Created by IntelliJ IDEA.
  User: student04
  Date: 4/10/20
  Time: 2:58 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Xavier's School</title>
</head>
<body>
<%@include file="WEB-INF/partials/navbar.jsp"%>

<form action="/school_login" method="post">
    <label for="username">
        Username
        <input type="text" id="username">
    </label><br>
    <label for="password">
        Password
        <input type="password" id="password">
    </label><br>
    <button type="submit">Log In</button>
</form>

<%@include file="WEB-INF/partials/footer.jsp"%>
</body>
</html>
