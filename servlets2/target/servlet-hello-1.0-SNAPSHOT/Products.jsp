<%--
  Created by IntelliJ IDEA.
  User: student04
  Date: 4/9/20
  Time: 3:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View Products</title>
    <%@include file="WEB-INF/partials/head.jsp" %>
</head>
<body>
<%@include file="WEB-INF/partials/navbar.jsp" %>
<div class="container">
    <div class="card mx-auto">
        <div class="card-header">
            <h1>Toys!</h1>
        </div>
        <div class="card-body">
            <c:forEach var="product" items="${products}">
                <div class="product">
                    <h2>${product.name}</h2>
                    <p>Price: $${product.price}</p>
                </div>
            </c:forEach>
            <a class="btn btn-block btn-outline-success" href="/products/create" role="button">Create-a-Product</a>
        </div>
        <%@include file="WEB-INF/partials/backButton.jsp" %>
    </div>
</div>
<%@include file="WEB-INF/partials/footer.jsp" %>
</body>
</html>
