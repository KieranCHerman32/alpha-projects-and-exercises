<%--
  Created by IntelliJ IDEA.
  User: student04
  Date: 4/9/20
  Time: 4:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>CurbBound</title>
    <%@include file="../WEB-INF/partials/head.jsp" %>
</head>
<body>
<%@include file="../WEB-INF/partials/navbar.jsp" %>
<div class="container">
    <div class="card">
        <div class="card-header">
            <h1>Available Items</h1>
        </div>
        <div class="card-body">
            <c:forEach var="item" items="${items}">
                <div class="item">
                    <h2>[${item.id}]${item.name}(${item.category})</h2>
                </div>
            </c:forEach>
        </div>
        <%@include file="../WEB-INF/partials/backButton.jsp" %>
    </div>
</div>
<%@include file="../WEB-INF/partials/footer.jsp" %>
</body>
</html>
