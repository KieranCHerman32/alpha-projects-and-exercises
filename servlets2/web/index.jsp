<%--
  Created by IntelliJ IDEA.
  User: student04
  Date: 4/3/20
  Time: 4:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%-- JSP directive - allows us to set conditions that apply to the entire jsp file --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%! int counter = -2; %>
<% counter += 1; %>
<html>
<head>
    <title>Servlet Exercises</title>
</head>
<body>
<%@include file="WEB-INF/partials/navbar.jsp"%>
<h4 align="center">Current Pageviews: <%= counter %></h4>
<%@include file="WEB-INF/partials/footer.jsp"%>
</body>
</html>
