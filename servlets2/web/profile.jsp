<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% request.setAttribute("name", "Kieran");%>

<html>
<head>
    <title>Profile</title>
</head>
<body>
<jsp:include page="./WEB-INF/partials/navbar.jsp"/>
<h3>Welcome back, ${sessionScope.user}</h3>
<jsp:include page="./WEB-INF/partials/footer.jsp"/>
</body>
</html>
