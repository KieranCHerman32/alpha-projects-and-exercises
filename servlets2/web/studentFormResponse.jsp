<%--
  Created by IntelliJ IDEA.
  User: student04
  Date: 4/8/20
  Time: 1:59 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student Confirmed</title>
</head>
<body>
<p>The User is confirmed: ${param.firstName} ${param.lastName}</p>
<p>User's Gender Identity: ${param.gender}</p>
<p>Programming Language Experience</p>
<ul>
    <%
        String[] languages = request.getParameterValues("progLang");
        for (String l : languages) {
            // Displaying selected languages dynamically //
            out.println("<li>" + l + "</li>");
        }
    %>
</ul><!-- Expression Language -->
</body>
</html>
