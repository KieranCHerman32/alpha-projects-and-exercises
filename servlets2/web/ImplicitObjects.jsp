<%--
  Created by IntelliJ IDEA.
  User: student04
  Date: 4/7/20
  Time: 10:06 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Implicit Objects</title>
</head>
<body>
<h1>Welcome to the implicit Object Site</h1>
<p>Path: <%= request.getRequestURL()%></p>
<p>Query String: <%= request.getQueryString()%></p>
<p>"name" Parameter: <%= request.getParameter("name")%></p>
<p>"method" Attribute: <%= request.getMethod()%></p>
<p>User-Agent Header: <%= request.getHeader("user-agent")%></p>

</body>
</html>
