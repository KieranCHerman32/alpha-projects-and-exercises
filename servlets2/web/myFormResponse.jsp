<%--
  Created by IntelliJ IDEA.
  User: student04
  Date: 4/8/20
  Time: 2:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student Registration</title>
    <jsp:include page="/WEB-INF/partials/head.jsp"/>
</head>
<body>
<%@include file="WEB-INF/partials/navbar.jsp" %>
<div class="container">
    <div class="card">
        <div class="card-header">
            <h3 align="center">Registration Confirmed</h3>
        </div>
        <div class="card-body">
            <p>Student Name: ${param.firstName} ${param.lastName}</p>
            <p>Registered to ${param.garden} GARDEN</p>
            <p>Weapons Training Tract: ${param.weapon}</p>
        </div>
        <%@include file="WEB-INF/partials/backButton.jsp"%>
    </div>
</div>
<%@include file="WEB-INF/partials/footer.jsp" %>
</body>
</html>
