import java.util.ArrayList;
import java.util.List;

public class ListItemsDao2 implements Items2 {
    public List<Item2> items;

    public List<Item2> all() {
        if (items == null) {
            items = generateItems();
        }
        return items;
    }

    private List<Item2> generateItems() {
        List<Item2> items = new ArrayList<>();
        items.add(new Item2(
                1,
                1,
                "Red Grapes",
                "Produce"
        ));
        items.add(new Item2(
                2,
                1,
                "2% Milk",
                "Produce"
        ));
        items.add(new Item2(
                3,
                2,
                "Steak",
                "Meat"
        ));
        items.add(new Item2(
                4,
                2,
                "Chocolate",
                "Snacks"
        ));
        return items;
    }
}
