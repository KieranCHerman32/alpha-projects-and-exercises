public class DAOFactoryProducts {
    private static Products productsDAO;

    public static Products getProductDAO() {
        if (productsDAO == null) {
            productsDAO = new ListProductsDAO();
        }
        return productsDAO;
    }
}
