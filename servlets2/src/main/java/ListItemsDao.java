import java.util.ArrayList;
import java.util.List;

public class ListItemsDao implements Items {
    public List<Item> items;

    public List<Item> all() {
        if (items == null) {
            items = generateItems();
        }
        return items;
    }

    private List<Item> generateItems() {
        List<Item> items = new ArrayList<>();
        items.add(new Item(
                1,
                1,
                "Red Grapes",
                "Produce"
        ));
        items.add(new Item(
                2,
                1,
                "Tomatoes",
                "Produce"
        ));
        items.add(new Item(
                3,
                2,
                "Lettuce",
                "Produce"
        ));
        items.add(new Item(
                4,
                2,
                "Mushrooms",
                "Produce"
        ));
        return items;
    }
}
