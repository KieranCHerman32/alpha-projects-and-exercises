import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "MasterRepoServlet", urlPatterns = "/masterRepo")
public class MasterRepoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getSession().getAttribute("adminUser").equals(false) || request.getSession().getAttribute("adminPass").equals(false)) {
            response.sendRedirect("/loginPage");
        } else if (request.getSession().getAttribute("adminUser") != null){
            request.getRequestDispatcher("/SCE/masterRepo.jsp").forward(request, response);
        }
    }
}
