public class DAOFactory {
    private static Items ItemsDao;

    public static Items getItemsDao() {
        if (ItemsDao == null) {
            ItemsDao = new ListItemsDao();
        }
        return ItemsDao;
    }
}
