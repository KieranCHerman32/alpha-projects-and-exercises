import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ShowProductsServlet", urlPatterns = "/products")
public class ShowProductsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Use factory to get DAO object
        Products productsDAO = DAOFactoryProducts.getProductDAO();

        // Method to get all the products DAO
        List<Product> products = productsDAO.all();

        // Passing data into the JSP
        request.setAttribute("products", products);
        request.getRequestDispatcher("Products.jsp").forward(request, response);
    }

}
