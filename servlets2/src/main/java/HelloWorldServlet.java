import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "HelloWorldServlet", urlPatterns = "/hello-world")
public class HelloWorldServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html"); // Setting contentType of the response being sent to client.
    //  response.setContentType("text/css");
    //  response.setContentType("image/gif");
        PrintWriter out = response.getWriter(); // enables you to write formatted data to an underlying Writer
        out.println("<h1>Hello World</h1>\n<h2>Made By: KC Herman</h2>"); // a statement that prints arguments passed.
    }
}
