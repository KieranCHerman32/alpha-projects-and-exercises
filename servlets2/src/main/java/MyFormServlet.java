import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "MyFormServlet", urlPatterns = "/MyFormServlet")
public class MyFormServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.printf("<p>Student Name: %s %s</p>", request.getParameter("firstName"), request.getParameter("lastName"));
        out.printf("\n<p>Student Registered to: %s GARDEN</P", request.getParameter("garden"));
        out.printf("\n<p>Preferred Weapons Training: %s</p>", request.getParameter("weapon"));
        out.println("</body></html>");
    }
}
