import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "SchoolLoginServlet", urlPatterns = "/school_login")
public class SchoolLoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 2 String Variables
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        // Created boolean to see if user entered admin credentials
        boolean isAdmin = username.equals("admin") && password.equals("admin");
        // Figure out if the login attempt is valid ...
        if (isAdmin) {
            request.getSession().setAttribute("isAdmin", true);
            response.sendRedirect("/admin");
        } else {
            response.sendRedirect("/school_login");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
