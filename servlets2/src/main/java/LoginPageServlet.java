import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "LoginPageServlet", urlPatterns = "/loginPage")
public class LoginPageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName = request.getParameter("username");
        String passWord = request.getParameter("password");
        boolean adminUser = userName.equals("admin");
        boolean adminPass = passWord.equals("admin");

        if (adminPass && adminUser) {
            request.getSession().setAttribute("adminPass", true);
            request.getSession().setAttribute("adminUser", true);
            response.sendRedirect("/masterRepo");
        } else {
            response.sendRedirect("/loginPage");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getSession().getAttribute("username") != null) {
            response.sendRedirect("/masterRepo");
        }
        request.getRequestDispatcher("/SCE/loginPage.jsp").forward(request, response);
    }
}
