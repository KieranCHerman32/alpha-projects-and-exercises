public class DaoFactory2 {
    private static Items2 itemsDao;

    public static Items2 getItemsDao() {
        if (itemsDao == null) {
            itemsDao = new ListItemsDao2();
        }
        return itemsDao;
    }
}
