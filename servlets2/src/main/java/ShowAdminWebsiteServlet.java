import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ShowAdminWebsiteServlet", urlPatterns = "/admin")
public class ShowAdminWebsiteServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Redirect if user is NOT an admin
        if (request.getSession().getAttribute("isAdmin").equals(false)) {
            response.sendRedirect("/school_login");
        } else {
            request.getRequestDispatcher("/admin").forward(request, response);
        }
    }
}
