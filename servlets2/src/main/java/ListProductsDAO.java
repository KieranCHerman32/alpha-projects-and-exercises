import java.util.ArrayList;
import java.util.List;

public class ListProductsDAO implements Products {
    private List<Product> products = new ArrayList<>();

    // WHen an instance of this Class is created,
    // we will populate the products arrayList with dummied data
    public ListProductsDAO() {
        insert(new Product("Jenga", 10.27));
        insert(new Product("Taco vs. Burrito", 19.99));
        insert(new Product("Adult Coloring Book", 19.16));
    }

    // Adding passed object into internal arrayList of products
    public void insert(Product product) {
        this.products.add(product);
    }

    public List<Product> all() {
        return this.products;
    }
}
