import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ShowItemsServlet", urlPatterns = "/curbside")
public class ShowItemsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    Items2 itemsDAO = DaoFactory2.getItemsDao();
        List<Item2> items = itemsDAO.all();
        request.setAttribute("items", items);
        request.getRequestDispatcher("/items/index.jsp").forward(request, response);
    }
}
