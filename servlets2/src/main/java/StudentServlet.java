import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "StudentServlet", urlPatterns = "/student")
public class StudentServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    // Step 1 - Set Content Type
        response.setContentType("text/html");
    // Step 2 - Get PrintWriter
        PrintWriter out = response.getWriter();
    // Step 3 - Dynamically Generate HTML Content
        out.println(
                "<html>" +
                "<body>");

        out.printf("\nThe Student is confirmed: %s %s", request.getParameter("firstName"), request.getParameter("lastName"));

        out.println(
                "</body>" +
                "</html>");
    }
}
