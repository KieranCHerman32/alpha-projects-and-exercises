import java.util.List;

public interface Products {
    List<Product> all();
    void insert(Product product); // Persist Product to the database

    // Interface describes the operations that our application can perform
}
