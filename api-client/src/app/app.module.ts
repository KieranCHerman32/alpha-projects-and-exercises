import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FilestackDemoComponent } from './filestack-demo/filestack-demo.component';
import { MathDemoComponent } from './math-demo/math-demo.component';
import { FilestackModule } from '@filestack/angular';

@NgModule({
  declarations: [
    AppComponent,
    FilestackDemoComponent,
    MathDemoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FilestackModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
