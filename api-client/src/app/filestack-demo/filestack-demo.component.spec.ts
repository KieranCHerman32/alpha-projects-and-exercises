import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilestackDemoComponent } from './filestack-demo.component';

describe('FilestackDemoComponent', () => {
  let component: FilestackDemoComponent;
  let fixture: ComponentFixture<FilestackDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilestackDemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilestackDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
