import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-filestack-demo',
  templateUrl: './filestack-demo.component.html',
  styleUrls: ['./filestack-demo.component.css']
})
export class FilestackDemoComponent implements OnInit {
  fileValue = null;
  defaultPath = 'https://cdn.filestackcontent.com/';
  fileUrl = null;

  uploadForm = new FormGroup({
    name: new FormControl(''),
    desc: new FormControl(''),
    url: new FormControl('')
  });

  constructor(private http: HttpClient) {
  }
  ngOnInit(): void {
  }

  onUploadSuccess(res: any) {
    console.log('###uploadSuccess', res);
    this.fileUrl = res.filesUploaded[0].url;
    console.log(this.fileUrl);
  }

  onUploadError(err: any) {
    console.log('###uploadError', err);
  }

  // formSubmission(object) {

  // }

  uploadSubmit() {
    this.fileValue = null;
    const requestObject = {
      name: this.uploadForm.get('name').value,
      description: this.uploadForm.get('desc').value,
      url: this.fileUrl
    };

    this.http.post(`http://localhost:8080/filestack/upload`, requestObject).toPromise()
    .then((response) => {
      this.fileValue = response;
    });
    console.log(requestObject.url);
  }
}
