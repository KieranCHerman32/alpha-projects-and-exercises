import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MathDemoComponent } from './math-demo.component';

describe('MathDemoComponent', () => {
  let component: MathDemoComponent;
  let fixture: ComponentFixture<MathDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MathDemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MathDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
