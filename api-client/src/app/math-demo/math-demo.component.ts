import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
​
@Component({
  selector: 'app-math-demo',
  templateUrl: './math-demo.component.html',
  styleUrls: ['./math-demo.component.css']
})
export class MathDemoComponent implements OnInit {
​
  calculatedValue = null;
  multCalculatedValue = null;
​
  additionForm = new FormGroup({
    num1: new FormControl(''),
    num2: new FormControl('')
  });
​
  multiplyForm = new FormGroup({
    num1: new FormControl(''),
    num2: new FormControl('')
  });
​
  constructor(private http: HttpClient){
  }
​
  ngOnInit(): void {
  }
​
  additionSubmit() {
    this.calculatedValue = null;
    this.http.get(`http://localhost:8080/math/add?num1=${this.additionForm.get('num1').value}
    &num2=${this.additionForm.get('num2').value}`).toPromise()
    .then((response) => {
      this.calculatedValue = response;
    });
  }
​
  multiplicationSubmit() {
    this.multCalculatedValue = null;
    const requestObject = {
      num1: this.multiplyForm.get('num1').value,
      num2: this.multiplyForm.get('num2').value
    };
​
    this.http.post(`http://localhost:8080/math/mult`, requestObject).toPromise()
      .then((response) => {
        this.multCalculatedValue = response;
      });
  }
​
}
