import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MathDemoComponent } from './math-demo/math-demo.component';
import { FilestackDemoComponent } from './filestack-demo/filestack-demo.component';


const routes: Routes = [
  { path: 'math', component: MathDemoComponent},
  { path: 'filestack', component: FilestackDemoComponent},
  { path: '',   redirectTo: '/filestack', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
